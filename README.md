# Membrane
a PHP Memcached Wrapper which supports SASL authentication

# Initial Setup and Configuration

## Requirements
- PHP 7.3 or above.
- [Memcached extention](https://www.php.net/manual/en/book.memcached.php) installed.


## Constants
Membrane does not have any required constants instead it offers some optional configuration options. 

`MEMBRANE_HOST`( `string`):    
The IP address or unix location of your Memcached server. Defaults to `127.0.0.1`


`MEMBRANE_PORT` ( `int` ):    
The port to use when connecting to the Memcached server. Defaults to `11211`

`MEMBRANE_BINARY` ( `bool` ):    
 Define this to `true` to use `Memcached::OPT_BINARY_PROTOCOL`. Default is `false` (binary off).

`MEMBRANE_SASL_USER`( `string` ):   
If you have set a user [Simple Authentication and Security Layer (SASL)](https://en.wikipedia.org/wiki/Simple_Authentication_and_Security_Layer) enabled you may use this option to set the user name.

For more on setting up SASL with Memcached see:
https://github.com/memcached/memcached/wiki/SASLHowto

`MEMBRANE_SASL_PASS`( `string` ):   
The password for your SASL user defined with `MEMBRANE_SASL_USER`. Both constants must be set before authentication will be used.


## Example Usage

    require_once 'membrane.class.php';
    use Membrane\Core as membrane;

    $mem = new membrane();
    $data = $mem->get('example');

    if (!$data) {
       $mem->set('example', 'hello world'); //defaults to raw and is stored for one hour
       $data = $mem->get('example');
    }
    echo ($data); // hello world


Set JSON data with up to one day expiration.
  
    $data = $mem->get('json');
    if (!$data) {
        $json = array( 'hello' => 'world')
        $mem->set('json', $json, 84600,'json');
        $data = $mem->get('json');
    }
    echo ($data); // {"hello":"world"}

## Methods
`$membrane->set($key, $data, $experiation, $type)` – Sets the data into memory.   
`$key`   : unique key name   
`$data`  : The Data to store   
`expire` : Max lengh to store the data, dedault to 0, no experation.   
`$type`  : Optional data type, used for escaping, supported types are `int`, `json`, `array`, with default to null/string.

`$membrane->get($key, $serial)` – Gets the data from a specific key.    
`$key`   : unique key name   
`$serial`   : should the data be unserialize (used with `array` datatype.)

`$membrane->delete($key)` – Delets data from a specific key.    
`$key`   : unique key name   


# Changelog
*1.1.0 2020-12-19*
- Add GPL 3+ License
- Add support for array type (seralized data)
- Update documentation

*1.0.0 2020-08-08*

 - Initial release

# Changelog


# License

    Membrane a PHP Memcached Wrapper which supports SASL authentication
    Copyright (C) 2020 Brooke. ( https://brooke.codes/contact )

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public Licensec
    along with this program. If not, see <http://www.gnu.org/licenses/>
