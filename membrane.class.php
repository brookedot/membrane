<?php

/*
Package: Membrane a PHP Memcached Wrapper
Version: 1.1
File: membrane.class.php
License: GPL-2.0+
(c) 2020 Brooke.
*/

namespace Membrane;

if (!class_exists('Core')) {
    /**
     * Core Class for using Memcached with PHP
     */
    class Core
    {
        private $memcache = null;
       
         /**
         * Connects to Memcache when class is called.
         *
         * @since  1.0.0
         * @access private
         */
        public function __construct()
        {
            // Bail with fatal if requirements arn't met.
            if (! $this->compatibilityChecks()) {
                return false;
            }
            $this->connect();
        }

        /**
         * Checks if PHP is greatet than 7.3 and the Memcached extention is installed.
         *
         * @since  1.0.0
         * @access private
         * @return bool
         */
        private function compatibilityChecks()
        {
            if (version_compare(PHP_VERSION, '7.3.0', '<')) {
                trigger_error('PHP 7.3 or above is required.', E_USER_ERROR);
                return false;
            }

            if (!extension_loaded('memcached') || !class_exists('Memcached')) {
                trigger_error('Memcached class is not loaded or does not exist', E_USER_ERROR);
                return false;
            }
            return true;
        }

        /**
         * Connect to the Memcached Server
         *
         * @since  1.0.0
         * @access protected
         * @return string
         */
        public function connect()
        {
            $port = (defined('MEMBRANE_PORT') && is_numeric('MEMBRANE_PORT')) ? (int)'MEMBRANE_PORT' : '11211';

            if (defined('MEMBRANE_HOST')) {
                $host = MEMBRANE_HOST;
                if (filter_var($host, FILTER_VALIDATE_IP)) {
                    $host = $host;
                } elseif (substr($host, 0, 7) == "unix://") {
                    $host = filter_var(substr($host, 7), FILTER_SANITIZE_URL);
                } else {
                    $host = null;
                    trigger_error('MEMBRANE_HOST is invalid, remove constant or set a valid host.', E_USER_WARNING);
                }
            } else {
                $host = '127.0.0.1';
            }

            $this->memcache = new \Memcached();
            
            if (defined('MEMBRANE_BINARY') && MEMBRANE_BINARY === true) {
                $this->memcache->setOption(\Memcached::OPT_BINARY_PROTOCOL, true);
            }
            if (defined('MEMBRANE_SASL_USER') && defined('MEMBRANE_SASL_PASS')) {
                $user = filter_var(MEMBRANE_SASL_USER, FILTER_SANITIZE_STRING);
                $pass = filter_var(MEMBRANE_SASL_PASS, FILTER_SANITIZE_STRING);
                
                $this->memcache->setSaslAuthData($user, $pass);
            }

            $this->memcache->addServer($host, $port)
                or die("Server could not connect to memcached!\n");
        }
        
        /**
         * Sanatizes the Key
         *
         * @since  1.0.0
         * @access private
         * @param string $key The Memcached key to sanatize.
         * @return string
         */
        private static function sanitizeKey($key)
        {
            return( filter_var($key, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH));
        }

         /**
         * Sanatizes the Data
         *
         * @since  1.0.0
         * @access private
         * @param string $data to be sanatized
         * @param string $type accepts int, json, or raw.
         * @return string
         */
        private static function sanitizeData($data, $type = '')
        {
            if (empty($type)) {
                return filter_var($data, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
            }
            if ($type === strtolower('json')) {
                return json_encode($data);
            }
            if ($type === strtolower('int')) {
                return (int)($data);
            }
            if ($type === strtolower('array')) {
                return base64_encode(serialize($data));
            }

            return filter_var($data, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_HIGH);
        }
 
        /**
         * Set the data and key into memory
         *
         * @since  1.0.0
         * @access public
         * @param string $key The Memcached key to set.
         * @param string $data to be stored into memory
         * @param int $expire The max value to store the key
         * @param string $type accepts int, json, or raw.
         * @return string
         */
        public function set($key, $data, $expire = 3600, $type = '')
        {
            $key = self::sanitizeKey($key);
            $expire = ((int)$expire !== 0) ? time() + (int)$expire : 0;
            $data = self::sanitizeData($data, $type);
            return $this->memcache->set($key, $data, $expire);
        }

        /**
         * Get a value by key
         *
         * @since  1.0.0
         * @access public
         * @param string $key The Memcached key to return.
         * @param string $serial Should the data be unserialize, such as in the case of storing an array, default false
         * @return string
         */
        public function get($key, $serial = false)
        {
            if ($serial) {
                return unserialize(base64_decode($this->memcache->get(self::sanitizeKey($key))), ['allowed_classes' => false]);
            }
            return $this->memcache->get(self::sanitizeKey($key));
        }

         /**
         * Delete data by key
         *
         * @since  1.0.0
         * @access public
         * @param string $key The Memcached key to delete.
         * @return string
         */
        public function delete($key)
        {
            return $this->memcache->delete(self::sanitizeKey($key));
        }
    }
}
